import './globals.css'
import type { Metadata } from 'next'
import { Inter } from 'next/font/google'
import { FC, ReactNode } from 'react'

const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: 'Dita Vrbová - Keramika',
  description: 'Galerie keramických produktů',
}

export type RootLayoutProps = {
  children: ReactNode,
};

export const RootLayout: FC<RootLayoutProps> = ({ children }) => {
  return (
    <html lang="cs">
      <body className={inter.className + " min-w-screen bg-red-900"}>
        <header className="container mx-auto text-center py-5">
          <h1 className="text-4xl text-white font-bold">Dita Vrbová</h1>
        </header>
        <div className="flex flex-col-reverse justify-center items-center md:flex-row md:items-start">
          <aside className="bg-red-900 md:flex-grow-0">
            Kontakt
          </aside>
          <main className="md:flex-grow">
            {children}
          </main>
        </div>
      </body>
    </html>
  )
}

export default RootLayout;
