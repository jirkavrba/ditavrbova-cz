import { NextPage } from 'next'
import directus from "@/api";
import { readItems } from '@directus/sdk';

const fetchProducts = async () => {
  return await directus.request(readItems("products", { fields: ["*.*"] }));
}

export const Home: NextPage = async () => {
  const products = await fetchProducts();

  return (
    <div>
      {JSON.stringify(products)}
    </div>
  );
};

export default Home;