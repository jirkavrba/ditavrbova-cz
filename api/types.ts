export type ProductImage = {
    readonly id: number,
    readonly directus_files_id: string,
};

export type Product = {
    readonly id: number,
    readonly name: string,
    readonly description: string,
    readonly slug: string,
    readonly in_stock: boolean,
    readonly images: Array<ProductImage>,
};

export type Schema = {
    products: Array<Product>,
};