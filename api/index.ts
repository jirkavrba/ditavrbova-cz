import { createDirectus, rest } from "@directus/sdk";
import { Schema } from "./types";

const directus = createDirectus<Schema>(process.env.DIRECTUS_URL || "https://directus.ditavrbova.cz").with(rest());

export default directus;